import '../styles/globals.css'
import '../styles/button.css'
import '../styles/bnt.css'
import '../styles/notification.css'
import { Provider } from 'react-redux'
import rootReducer  from '../reducer/store'
import { createStore } from 'redux'
import { CookiesProvider } from "react-cookie"

 
const store = createStore(rootReducer)

function MyApp({ Component, pageProps }) {
  return  <CookiesProvider><Provider store={store} ><Component {...pageProps} />
</Provider></CookiesProvider>}

export default MyApp
