var data = () => {
  return (data = [
    {
      type: 'SASHIMI',
      id: 103,
      price: 899,
      nameEng: 'SASHIMI ISO MORI',
      nameThai: 'ซาซิมิ  อิดโซะ โมริ',
      details: 'ปลาดิบรวม7อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 101,
      price: 499,
      nameEng: 'SASHIMI GOSHU MORI',
      nameThai: 'ซาซิมิ โกชุ โมริ',
      details: 'ปลาดิบรวม5อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 102,
      price: 249,
      nameEng: 'SASHIMI SANSHU MORI',
      nameThai: 'ซาซิมิ ซานชุ โมริ',
      details: 'ปลาดิบรวม3อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 100,
      price: 1499,
      nameEng: 'TOKUJO SASHIMI MORI',
      nameThai: 'โทคุโจ ซาซิมิ โมริ',
      details: 'ปลาดิบรวมพิเศษ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 104,
      price: 169,
      nameEng: 'NAMAGAKI ',
      nameThai: 'นามากากิ',
      details: 'หอยนางรมสด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 120,
      price: 179,
      nameEng: 'AKAEBI SASHIMI',
      nameThai: 'อะกาอิบิ ซาชิมิ',
      details: 'กุ้งแดงสดทะเล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 112,
      price: 199,
      nameEng: 'SALMON SASHIMI',
      nameThai: 'แซลมอล ซาชิมิ',
      details: 'ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 113,
      price: 199,
      nameEng: 'ABURI SALMON SASHIMI',
      nameThai: 'อะบุริ แซลมอล ซาชิมิ',
      details: 'ปลาแซลมอนรมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 117,
      price: 199,
      nameEng: 'SALMON USUDUKURI',
      nameThai: 'แซลมอล อุซูดุคุริ',
      details: 'ปลาแซลมอนแล่บาง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 105,
      price: 179,
      nameEng: 'MAGURO SASHIMI',
      nameThai: 'มากุโร่ ซาชิมิ',
      details: 'ปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 119,
      price: 169,
      nameEng: 'ABURI SHIMESABA SASHIMI',
      nameThai: 'อะบุริ ชิเมะซาบะ ซาชิมิ',
      details: 'ปลาซาบะดองรมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 106,
      price: 299,
      nameEng: 'HOTATE SASHIMI',
      nameThai: 'โฮตาเตะ ซาชิมิ',
      details: 'หอยเชลล์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 109,
      price: 159,
      nameEng: 'AORI IKA SASHIMI',
      nameThai: 'ออริ อิกะ ซาชิมิ',
      details: 'ปลาหมึกหอมสไลด์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 107,
      price: 299,
      nameEng: 'HAMACHI SASHIMI',
      nameThai: 'ฮามาจิ ซาชิมิ',
      details: 'ปลาฮามาจิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 111,
      price: 89,
      nameEng: 'KANIKAMA SASHIMI',
      nameThai: 'คานิคามา ซาชิมิ',
      details: 'ปูอัด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 114,
      price: 699,
      nameEng: 'OTORO SASHIMI',
      nameThai: 'โอโทโร่ ซาชิมิ',
      details: 'ราวท้องปลาทูน่า(โอโทโร่)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 121,
      price: 159,
      nameEng: 'IKA SOMEN',
      nameThai: 'อิกะ โซเมง',
      details: 'ปลาหมึกหอมปรุงรส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SASHIMI',
      id: 124,
      price: 159,
      nameEng: 'SHIROMI SASHIMI',
      nameThai: 'ชิโรมิ ซาซิมิ',
      details: 'ปลาเนื้อขาวซาซิมิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 500,
      price: 139,
      nameEng: 'CALIFORNIA ROLL GOMA',
      nameThai: 'แคลิฟอร์เนีย โรล โกมะ',
      details: 'แคลิฟอร์เนียโรล (คลุกงา)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 501,
      price: 149,
      nameEng: 'CALIFORNIA ROLL EBIKO',
      nameThai: 'แคลิฟอร์เนีย โรล อิบิโกะ',
      details: 'แคลิฟอร์เนียโรล (คลุกไข่กุ้ง)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 504,
      price: 199,
      nameEng: 'PHILADELPHIA ROLL',
      nameThai: 'ฟิลาเดลเฟียโรล',
      details: 'ฟิลาเดลเฟียโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 513,
      price: 119,
      nameEng: 'TORIKARA ROLL',
      nameThai: 'โทริคารา โรล',
      details: 'ไก่ทอดคาราอาเกะโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 503,
      price: 179,
      nameEng: 'EBI TEN ROLL',
      nameThai: 'อิบิ เทน โรล',
      details: 'กุ้งเทมปุระโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 502,
      price: 359,
      nameEng: 'UNAGI AVOCADO ROLL',
      nameThai: 'อูนากิ อะโวคาโด โรล',
      details: 'ปลาไหลย่างและอะโวคาโดโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 505,
      price: 249,
      nameEng: 'TUNA SPECIAL ROLL',
      nameThai: 'ทูน่า สเปเชียล โรล',
      details: 'ทูน่าสเปเชียลโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 506,
      price: 179,
      nameEng: 'SPICY TUNA ROLL',
      nameThai: 'สไปร์ซี่ ทูน่า โรล',
      details: 'สไปร์ซี่ทูน่าโรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 515,
      price: 319,
      nameEng: 'SALMON DELUX ROLL',
      nameThai: 'แซลมอล ดีลักซ์ โรล',
      details: 'แซลมอนดีลักซ์โรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 520,
      price: 199,
      nameEng: 'RAINBOW ROLL',
      nameThai: 'เรนโบว์ โรล',
      details: 'เรนโบว์โรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 521,
      price: 1399,
      nameEng: 'DRAGON ROLL',
      nameThai: 'ดราก้อน โรล',
      details: 'ปลาไหลยักษ์โรล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'ROLL SUSHI',
      id: 523,
      price: 249,
      nameEng: 'BEEF ROLL',
      nameThai: 'บีฟโรล',
      details: 'โรลเนื้อ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 450,
      price: 69,
      nameEng: 'SALMON TEMAKI',
      nameThai: 'แซลมอล เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 471,
      price: 75,
      nameEng: 'SALMON EBIKO TEMAKI',
      nameThai: 'แซลมอล อิบิโกะ เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอนและไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 451,
      price: 129,
      nameEng: 'SALMON IKURA TEMAKI',
      nameThai: 'แซลมอล อิกุระ เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอนและไข่ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 452,
      price: 79,
      nameEng: 'SALMON AVOCADO TEMAKI',
      nameThai: 'แซลมอล อโวคาโด เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอนและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 453,
      price: 74,
      nameEng: 'SALMON MAYO TEMAKI',
      nameThai: 'แซลมอล มาโย เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอนราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI SUSHI',
      id: 454,
      price: 79,
      nameEng: 'SALMON CHEESE TEMAKI',
      nameThai: 'แซลมอล ชีส เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอนราดชีสย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 455,
      price: 49,
      nameEng: 'EBI TEMAKI',
      nameThai: 'อิบิ เตมากิ',
      details: 'ข้าวห่อสาหร่ายกุ้งต้ม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 456,
      price: 59,
      nameEng: 'EBI EBIKO TEMAKI',
      nameThai: 'อิบิ อิบิโกะ เตมากิ',
      details: 'ข้าวห่อสาหร่ายกุ้งต้มและไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 457,
      price: 59,
      nameEng: 'EBI AVOCADO TEMAKI',
      nameThai: 'อิบิ อะโวคาโด เตมากิ',
      details: 'ข้าวห่อสาหร่ายกุ้งต้มและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 458,
      price: 55,
      nameEng: 'EBI MAYO TEMAKI',
      nameThai: 'อิบิ มาโย เตมากิ',
      details: 'ข้าวห่อสาหร่ายกุ้งต้มราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 459,
      price: 59,
      nameEng: 'EBI CHEESE TEMAKI',
      nameThai: 'อิบิ ชีส เตมากิ',
      details: 'ข้าวห่อสาหร่ายกุ้งต้มและครีมชีส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 460,
      price: 49,
      nameEng: 'KANIKAMA TEMAKI',
      nameThai: 'คานิคามา เตมากิ',
      details: 'ข้าวห่อสาหร่ายปูอัด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 461,
      price: 59,
      nameEng: 'KANIKAMA EBIKO TEMAKI',
      nameThai: 'คานิคามา อิบิโกะ เตมากิ',
      details: 'ข้าวห่อสาหร่ายปูอัดและไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 462,
      price: 59,
      nameEng: 'KANIKAMA AVOCADO TEMAKI',
      nameThai: 'คานิคามา อะโวคาโด เตมากิ',
      details: 'ข้าวห่อสาหร่ายปูอัดและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 463,
      price: 49,
      nameEng: 'KANIKAMA MAYO TEMAKI',
      nameThai: 'คานิคามา มาโย เตมากิ',
      details: 'ข้าวห่อสาหร่ายปูอัดราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 464,
      price: 59,
      nameEng: 'KANIKAMA CHEESE TEMAKI',
      nameThai: 'คานิคามา ชีส เตมากิ',
      details: 'ข้าวห่อสาหร่ายปูอัดและครีมชีส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 465,
      price: 49,
      nameEng: 'MAGURO TEMAKI',
      nameThai: 'มากูโร่ เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 466,
      price: 59,
      nameEng: 'MAGURO AVOCADO TEMAKI',
      nameThai: 'มากูโร่ อะโวคาโด เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่าและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 467,
      price: 55,
      nameEng: 'ABURI MAGURO MAYO TEMAKI',
      nameThai: 'อะบุริ มากูโร่ มาโย เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่ารมควันราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 468,
      price: 65,
      nameEng: 'ABURI MAGURO CHEESE TEMAKI',
      nameThai: 'อะบุริ มากูโร่ ชีส เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่าและครีมชีส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 469,
      price: 129,
      nameEng: 'IKURA TEMAKI',
      nameThai: 'อิกุระ เตมากิ',
      details: 'ข้าวห่อสาหร่ายไข่ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 472,
      price: 129,
      nameEng: 'IKURA KYURI TEMAKI',
      nameThai: 'อิกุระ เคียวยูริ เตมากิ',
      details: 'ข้าวห่อสาหร่ายไข่ปลาแซลมอนและแตงกวา',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 470,
      price: 249,
      nameEng: 'OTORO TEMAKI',
      nameThai: 'โอโทโร่ เตมากิ',
      details: 'ข้าวห่อสาหร่ายราวท้องปลาทูน่า(โอโทโร่)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 474,
      price: 89,
      nameEng: 'IKA EBIKO TEMAKI',
      nameThai: 'อิกะ อิบิโกะ เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาหมึกหอมและไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 475,
      price: 49,
      nameEng: 'TUNA MAYO TEMAKI',
      nameThai: 'ทูน่า มาโย เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่าผสมมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMAKI　SUSHI',
      id: 473,
      price: 79,
      nameEng: 'IKA MENTAI TEMAKI',
      nameThai: 'อิกะ เมนไต เตมากิ',
      details: 'ข้าวห่อสาหร่ายปลาหมึกหอมผสมไข่ปลาเมนไตโกะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 550,
      price: 109,
      nameEng: 'TEKKA MAKI',
      nameThai: 'เต็กกะ มากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 551,
      price: 139,
      nameEng: 'SALMON MAKI',
      nameThai: 'แซลมอล มากิ',
      details: 'ข้าวห่อสาหร่ายปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 552,
      price: 69,
      nameEng: 'KYURI MAKI',
      nameThai: 'เคียวยูริ มากิ',
      details: 'ข้าวห่อสาหร่ายแตงกวา',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 558,
      price: 199,
      nameEng: 'NEGITORO MAKI',
      nameThai: 'เนกิโทโร มากิ',
      details: 'ข้าวห่อสาหร่ายราวท้องปลาทูน่าสับโรยต้นหอมซอย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 556,
      price: 119,
      nameEng: 'AVOCADO MAKI',
      nameThai: 'อะโวคาโด มากิ',
      details: 'ข้าวห่อสาหร่ายอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 553,
      price: 79,
      nameEng: 'NATTO MAKI',
      nameThai: 'นัตโตะ มากิ',
      details: 'ข้าวห่อสาหร่ายถั่วหมัก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOSOMAKI',
      id: 560,
      price: 89,
      nameEng: 'TUNA MAYO MAKI',
      nameThai: 'ทูน่า มาโย มากิ',
      details: 'ข้าวห่อสาหร่ายปลาทูน่าคลุกมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'OPEN INARI',
      id: 561,
      price: 79,
      nameEng: 'SALMON INARI',
      nameThai: 'แซลมอล อินาริ',
      details: 'ข้าวห่อเต้าหู้ทอดหน้าปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'OPEN INARI',
      id: 562,
      price: 99,
      nameEng: 'HAMACHI INARI',
      nameThai: 'ฮามาจิ อินาริ',
      details: 'ข้าวห่อเต้าหู้ทอดหน้าปลาฮามาจิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'OPEN INARI',
      id: 563,
      price: 249,
      nameEng: 'OTORO INARI',
      nameThai: 'โอโทโร่ อินาริ',
      details: 'ข้าวห่อเต้าหู้ทอดหน้าปลาโอโทโร่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'OPEN INARI',
      id: 564,
      price: 69,
      nameEng: 'AVOCADO INARI',
      nameThai: 'อะโวคาโด อินาริ',
      details: 'ข้าวห่อเต้าหู้ทอดหน้าอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHIMORIAWASE',
      id: 400,
      price: 199,
      nameEng: 'NIGIRI NANAKANMORI',
      nameThai: 'นิกิริ นานาคันโมริ',
      details: 'ซูชิรวม 7 อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHIMORIAWASE',
      id: 401,
      price: 499,
      nameEng: 'JO NIGIRI NANAKANMORI',
      nameThai: 'โจ นิกิริ นานาคันโมริ',
      details: 'ซูชิพิเศษ 7 อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHIMORIAWASE',
      id: 403,
      price: 139,
      nameEng: 'SALMON SANSHUMORI',
      nameThai: 'แซลมอล ซานซู โมริ',
      details: 'ซูชิปลาแซลมอนรวม 3 อย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHIMORIAWASE',
      id: 402,
      price: 749,
      nameEng: 'NIGIRI SUSHI 14 PIECES',
      nameThai: 'นิกิริ ซูชิ 14 ชิ้น',
      details: 'ซูชิรวม 14 ชิ้น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHIMORIAWASE',
      id: 428,
      price: 1599,
      nameEng: 'TOKUJO NIGIRI SUSHI 21 PIECES ',
      nameThai: 'โทคูโจ นิกิริ ซูชิ 21ชิ้น',
      details: 'ซูชิพิเศษ 21 ชิ้น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 405,
      price: 75,
      nameEng: 'MAGURO',
      nameThai: 'มากูโร่',
      details: 'ข้าวปั้นหน้าปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 435,
      price: 75,
      nameEng: 'ABURI MAGURO',
      nameThai: 'อะบูริ มากูโร่',
      details: 'ข้าวปั้นหน้าปลาทูน่ารมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 406,
      price: 89,
      nameEng: 'SALMON',
      nameThai: 'แซลมอล',
      details: 'ข้าวปั้นหน้าปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 407,
      price: 89,
      nameEng: 'ABURI SALMON',
      nameThai: 'อะบูริ แซลซอล',
      details: 'ข้าวปั้นหน้าปลาแซลมอนรมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 408,
      price: 95,
      nameEng: 'ONION SALMON',
      nameThai: 'โอเนี่ยน แซลมอล',
      details: 'ข้าวปั้นหน้าปลาแซลมอนราดหอมใหญ่มายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 409,
      price: 119,
      nameEng: 'HAMACHI',
      nameThai: 'ฮามาจิ',
      details: 'ข้าวปั้นหน้าปลาฮามาจิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 439,
      price: 99,
      nameEng: 'RED SHRIMP',
      nameThai: 'เรด เชิม',
      details: 'ข้าวปั้นหน้ากุ้งแดงสดทะเล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 413,
      price: 59,
      nameEng: 'TAMAGO',
      nameThai: 'ทามาโกะ',
      details: 'ข้าวปั้นหน้าไข่หวาน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 421,
      price: 49,
      nameEng: 'KANIKAMA',
      nameThai: 'คานิคามา',
      details: 'ข้าวปั้นหน้าปูอัด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'GUNKAN',
      id: 419,
      price: 59,
      nameEng: 'EBIKO',
      nameThai: 'อิบิโกะ',
      details: 'ข้าวปั้นห่อสาหร่ายไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 411,
      price: 79,
      nameEng: 'AORI IKA',
      nameThai: 'ออริ อิกะ',
      details: 'ข้าวปั้นหน้าปลาหมึกหอม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 412,
      price: 69,
      nameEng: 'EBI',
      nameThai: 'อิบิ',
      details: 'ข้าวปั้นหน้ากุ้งต้ม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 418,
      price: 79,
      nameEng: 'SHIME SABA',
      nameThai: 'ชิเมะ ซาบะ',
      details: 'ข้าวปั้นหน้าปลาซาบะดอง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 414,
      price: 189,
      nameEng: 'UNAGI',
      nameThai: 'อุนากิ',
      details: 'ข้าวปั้นหน้าปลาไหลย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 415,
      price: 159,
      nameEng: 'HOTATE',
      nameThai: 'โฮตาเตะ',
      details: 'ข้าวปั้นหน้าหอยเชลล์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 416,
      price: 159,
      nameEng: 'ABURI HOTATE',
      nameThai: 'อะบุริ โฮตาเตะ',
      details: 'ข้าวปั้นหน้าหอยเชลล์รมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'GUNKAN',
      id: 417,
      price: 189,
      nameEng: 'IKURA',
      nameThai: 'อิกุระ',
      details: 'ข้าวปั้นห่อสาหร่ายไข่ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 424,
      price: 99,
      nameEng: 'MAGURO AVOCADO',
      nameThai: 'มากูโร่ อะโวคาโด',
      details: 'ข้าวปั้นหน้าปลาทูน่าและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 425,
      price: 109,
      nameEng: 'SALMON AVOCADO',
      nameThai: 'แซลมอล อะโวคาโด',
      details: 'ข้าวปั้นหน้าปลาแซลมอนและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 431,
      price: 89,
      nameEng: 'TAKO',
      nameThai: 'ทาโกะ',
      details: 'ข้าวปั้นหน้าปลาหมึกยักษ์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 433,
      price: 69,
      nameEng: 'SHIROMI',
      nameThai: 'ชิโรมิ',
      details: 'ข้าวปั้นหน้าปลาเนื้อขาว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'GUNKAN',
      id: 434,
      price: 49,
      nameEng: 'TUNA MAYO',
      nameThai: 'ทูน่า มาโย',
      details: 'ข้าวปั้นห่อสาหร่ายปลาทูน่าคลุกมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 436,
      price: 79,
      nameEng: 'EBI CHEESE',
      nameThai: 'อิบิ ชีส',
      details: 'ข้าวปั้นหน้ากุ้งต้มราดชีสย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 438,
      price: 99,
      nameEng: 'SALMON CHEESE',
      nameThai: 'แซลมอล ชีส',
      details: 'ข้าวปั้นหน้าปลาแซลมอนราดชีสย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 427,
      price: 249,
      nameEng: 'WAGYU',
      nameThai: 'วากิว',
      details: 'ข้าวปั้นหน้าเนื้อวากิวย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 422,
      price: 449,
      nameEng: 'OTORO',
      nameThai: 'โอโทโร่',
      details: 'ข้าวปั้นหน้าราวท้องปลาทูน่า(โอโทโร่)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 423,
      price: 449,
      nameEng: 'OTORO ABURI',
      nameThai: 'โอโทโร่ อะบูริ',
      details: 'ข้าวปั้นหน้าราวท้องปลาทูน่ารมควัน(โอโทโร่)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 441,
      price: 39,
      nameEng: 'INARI',
      nameThai: 'อินาริ',
      details: 'ข้าวห่อเต้าหู้ทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 426,
      price: 89,
      nameEng: 'EBI AVOCADO',
      nameThai: 'อิบิ อะโวคาโด',
      details: 'ข้าวปั้นหน้ากุ้งต้มและอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SUSHI',
      id: 437,
      price: 85,
      nameEng: 'AVOCADO MAYO',
      nameThai: 'อะโวคาโด มาโย',
      details: 'ข้าวปั้นห่อสาหร่ายอะโวคาโดราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'GUNKAN',
      id: 442,
      price: 49,
      nameEng: 'WAKAME',
      nameThai: 'วากาเมะ',
      details: 'ข้าวปั้นห่อสาหร่ายหน้ายำสาหร่าย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'GUNKAN',
      id: 443,
      price: 39,
      nameEng: 'CORN MAYO',
      nameThai: 'คอร์น มาโย',
      details: 'ข้าวปั้นห่อสาหร่ายหน้าข้าวโพดราดมายองเนส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SALAD',
      id: 251,
      price: 199,
      nameEng: 'SEAFOOD CAESAR SALAD',
      nameThai: 'ซีฟู๊ด ซีซ่า สลัด',
      details: 'ซีซ่าสลัดซีฟู้ด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SALAD',
      id: 252,
      price: 89,
      nameEng: 'POTATO SALAD',
      nameThai: 'โปเตโต้ สลัด',
      details: 'สลัดมันฝรั่ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SALAD',
      id: 255,
      price: 189,
      nameEng: 'TOFU AVOCADO SALAD',
      nameThai: 'โตฟู่ อะโวคาโด สลัด',
      details: 'สลัดเต้าหู้และอะโวคาโด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SALAD',
      id: 253,
      price: 69,
      nameEng: 'KANIKAMA SALAD',
      nameThai: 'คานิคามา สลัด',
      details: 'สลัดปูอัด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SALAD',
      id: 254,
      price: 69,
      nameEng: 'TUNA CORN SALAD',
      nameThai: 'ทูน่า คอร์น สลัด',
      details: 'สลัดปลาทูน่ากับข้าวโพด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 200,
      price: 89,
      nameEng: 'TAMAGO ATE',
      nameThai: 'ทามาโกะ อะเตะ',
      details: 'ไข่หวานม้วน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 203,
      price: 59,
      nameEng: 'HIYAYAKKO',
      nameThai: 'ฮิยายักโกะ',
      details: 'เต้าหู้เย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 202,
      price: 89,
      nameEng: 'HIYASHI WAKAME',
      nameThai: 'ฮิยาชิ วากาเมะ',
      details: 'ยำสาหร่ายเย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 214,
      price: 59,
      nameEng: 'NATTO',
      nameThai: 'นัตโตะ',
      details: 'ถั่วหมัก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 211,
      price: 99,
      nameEng: 'CHAWANMUSHI',
      nameThai: 'ชะวานมูชิ',
      details: 'ไข่ตุ๋น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 201,
      price: 79,
      nameEng: 'EDAMAME',
      nameThai: 'อิดามาเมะ',
      details: 'ถั่วแระญี่ปุ่นต้ม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 334,
      price: 89,
      nameEng: 'SPICY EDAMAME',
      nameThai: 'สไปร์ซี่ อิดามาเมะ',
      details: 'ถั่วแระญี่ปุ่นคลุกซอสเผ็ด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 204,
      price: 99,
      nameEng: 'IKAMENTAI',
      nameThai: 'อิกะเมนไต',
      details: 'ปลาหมึกผสมไข่ปลาเมนไตโกะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KOBACHI',
      id: 226,
      price: 99,
      nameEng: 'IKANATTO',
      nameThai: 'อิกะนัตโตะ',
      details: 'ปลาหมึกหอมและนัตโตะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 216,
      price: 129,
      nameEng: 'KUSHINSAI TAMAGOTOJI',
      nameThai: 'กุชินไซ ทามาโกะโทจิ',
      details: 'ผักบุ้งผัดใส่ไข่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 218,
      price: 129,
      nameEng: 'MAGURO YUKKE',
      nameThai: 'มากูโร่ ยูกะเกะ',
      details: 'ยำปลาทูน่าสไตล์เกาหลี',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 219,
      price: 139,
      nameEng: 'SALMON YUKKE',
      nameThai: 'แซลมอล ยูกะกะ',
      details: 'ยำปลาแซลมอนสไตล์เกาหลี',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 313,
      price: 199,
      nameEng: 'BEEF TATAKI',
      nameThai: 'บีฟ ทาทากิ',
      details: 'เนื้อย่าง (เนื้อกึ่งสุกกึ่งดิบข้างในแดง 70%)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 319,
      price: 89,
      nameEng: 'POTATO FRY',
      nameThai: 'โปเตโต้ ฟราย',
      details: 'เฟรนฟราย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 320,
      price: 129,
      nameEng: 'WAKADORI KARAAGE',
      nameThai: 'วากาโดริ คาราอะเกะ',
      details: 'ไก่ทอดสไตล์ญี่ปุ่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 303,
      price: 119,
      nameEng: 'TAKO YAKI',
      nameThai: 'ทาโกะ ยากิ',
      details: 'ทาโกะยากิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 331,
      price: 99,
      nameEng: 'AGEDASHI TOFU',
      nameThai: 'เกะดาชิ โตฟู่',
      details: 'เต้าหู้ทอดราดซุปดาชิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 318,
      price: 129,
      nameEng: 'AGE GYOZA',
      nameThai: 'เกะ เกี๊ยวซ่า',
      details: 'เกี๊ยวซ่าทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 335,
      price: 139,
      nameEng: 'MENTAI CHEESE TAMAGOYAKI',
      nameThai: 'เมนไต ชีส ทามาโกะยากิ',
      details: 'ไข่ห่อไข่ปลาเมนไตโกะย่างชีส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 328,
      price: 129,
      nameEng: 'DASHIMAKI TAMAGO',
      nameThai: 'ดาชิมากิ ทามาโกะ',
      details: 'ไข่ม้วนย่างสไตล์ญี่ปุ่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 329,
      price: 119,
      nameEng: 'SALMON ARADAKI',
      nameThai: 'แซลมอล อะราดะกิ',
      details: 'หัวปลาแซลมอนต้มซีอิ๊ว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 300,
      price: 129,
      nameEng: 'CHICKEN TERI YAKI',
      nameThai: 'ชิกเค้น เทริ ยากิ',
      details: 'ไก่ย่างซอสเทริยากิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 301,
      price: 149,
      nameEng: 'KATSUTOJI',
      nameThai: 'คัตสึโทจิ',
      details: 'หมูชุบแป้งทอดโปะไข่ราดซีอิ๊ว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 302,
      price: 129,
      nameEng: 'YAKI GYOZA',
      nameThai: 'ยากิ เกี๊ยวซ่า',
      details: 'เกี๊ยวซ่าย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 305,
      price: 129,
      nameEng: 'SABA SHIO YAKI',
      nameThai: 'ซาบะ ชิโอ ยากิ',
      details: 'ปลาซาบะย่างเกลือ ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 306,
      price: 139,
      nameEng: 'SABA TERI YAKI',
      nameThai: 'ซาบะ เทริยากิ',
      details: 'ปลาซาบะย่างซีอิ๊ว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 307,
      price: 219,
      nameEng: 'SALMON SHIO YAKI',
      nameThai: 'แซลมอล ชิโอ ยากิ',
      details: 'ปลาแซลมอนย่างเกลือ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 308,
      price: 229,
      nameEng: 'SALMON TERI YAKI',
      nameThai: 'แซลมอล เทริยากิ',
      details: 'ปลาแซลมอนย่างซีอิ๊ว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 311,
      price: 749,
      nameEng: 'UNAGI KABAYAKI',
      nameThai: 'อูนากิ คาบายากิ',
      details: 'ปลาไหลย่างราดซอสหวาน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 385,
      price: 249,
      nameEng: 'IKA ICHIYABOSHI',
      nameThai: 'อิกะ อิชิยาโบชิ',
      details: 'ปลาหมึกย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 341,
      price: 129,
      nameEng: 'BUTABARA MOYASHI ITAME',
      nameThai: 'บูตะบาระ โมยาชิ อิทาเมะ',
      details: 'หมูสามชั้นผัดถั่วงอก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 312,
      price: 199,
      nameEng: 'BUTA KAKUNI',
      nameThai: 'บูตะ คาคูนิ',
      details: 'หมูสามชั้นตุ๋น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 309,
      price: 259,
      nameEng: 'HOTATE BUTTER YAKI',
      nameThai: 'โฮตาเตะ บัสเตอ ยากิ',
      details: 'หอยเชลล์ย่างเนย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 349,
      price: 129,
      nameEng: 'GESO KARAAGE',
      nameThai: 'เกโซะ คาราอะเกะ',
      details: 'หมึกทอดญี่ปุ่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 322,
      price: 129,
      nameEng: 'TONKATSU',
      nameThai: 'ทงคัตสึ',
      details: 'หมูทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 317,
      price: 999,
      nameEng: 'WAGYU STEAK',
      nameThai: 'วากิว สเต็ก',
      details: 'สเต๊กเนื้อวากิว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 332,
      price: 159,
      nameEng: 'SALMON HARASUYAKI',
      nameThai: 'แซลมอล ฮาราซุยากิ',
      details: 'ราวท้องปลาแซลมอนย่างเกลือ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'IPPIN',
      id: 344,
      price: 129,
      nameEng: 'KAISEN CHIJIMI',
      nameThai: 'ไคเซน ชิจิมิ',
      details: 'ซีฟู้ดชิจิมิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMPURA',
      id: 350,
      price: 139,
      nameEng: 'TEMPURA MORIAWASE',
      nameThai: 'เทมปุระ โมริอะวะเซะ',
      details: 'เทมปุระรวม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMPURA',
      id: 351,
      price: 199,
      nameEng: 'EBI TEMPURA',
      nameThai: 'อิบิ เทมปุระ',
      details: 'กุ้งทอดเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMPURA',
      id: 383,
      price: 149,
      nameEng: 'IKA TEMPURA',
      nameThai: 'อิกะ เทมปุระ',
      details: 'ปลาหมึกหอมทอดเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMPURA',
      id: 352,
      price: 119,
      nameEng: 'KUSHINSAI TEMPURA',
      nameThai: 'คูชินไซ เทมปุระ',
      details: 'ผักบุ้งเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'TEMPURA',
      id: 384,
      price: 149,
      nameEng: 'SHIROMI TEMPURA',
      nameThai: 'ชิโรมิ เทมปุระ',
      details: 'ปลาเนื้อขาวทอดเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 353,
      price: 149,
      nameEng: 'KUSHIKATSU MORIAWASE',
      nameThai: 'คุชิคัตสึ โมริอะวะเซะ',
      details: 'คุชิคัตสึรวม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 354,
      price: 19,
      nameEng: 'TORIMOMO KUSHI',
      nameThai: 'โทริโมะโมะ คุชิ',
      details: 'ไก่ชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 355,
      price: 19,
      nameEng: 'BUTA KUSHI',
      nameThai: 'บูตะ คุชิ',
      details: 'หมูชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 356,
      price: 19,
      nameEng: 'SHITAKE KUSHI',
      nameThai: 'ชิทาเกะ คุชิ',
      details: 'เห็ดหอมชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 358,
      price: 19,
      nameEng: 'TOMATO KUSHI',
      nameThai: 'โทมาโท คุชิ',
      details: 'มะเขือเทศชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 381,
      price: 19,
      nameEng: 'SALMON KAWA KUSHI',
      nameThai: 'แซลมอล คาวา คุชิ',
      details: 'หนังปลาแซลมอนชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 375,
      price: 19,
      nameEng: 'RENKON KUSHI',
      nameThai: 'เรนก้อน คูชิ',
      details: 'รากบัวชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 360,
      price: 29,
      nameEng: 'MAGURO KUSHI',
      nameThai: 'มากุโร่ คูชิ',
      details: 'ปลาทูน่าชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 361,
      price: 29,
      nameEng: 'CHEESE KUSHI',
      nameThai: 'ชีส คูชิ',
      details: 'ชีสชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 362,
      price: 39,
      nameEng: 'SALMON KUSHI',
      nameThai: 'แซลมอล คูชิ',
      details: 'ปลาแซลมอนชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 363,
      price: 39,
      nameEng: 'IKA KUSHI',
      nameThai: 'อิกะ คูชิ',
      details: 'หมึกหอมชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 366,
      price: 39,
      nameEng: 'KAKI KUSHI',
      nameThai: 'คากิ คูชิ',
      details: 'หอยนางรมชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 365,
      price: 49,
      nameEng: 'EBI KUSHI',
      nameThai: 'อิบิ คูชิ',
      details: 'กุ้งชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 374,
      price: 59,
      nameEng: 'HOTATE KUSHI',
      nameThai: 'โฮตาเตะ คูชิ',
      details: 'หอยเชลล์ชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 367,
      price: 79,
      nameEng: 'UNAGI KUSHI',
      nameThai: 'อุนากิ คูชิ',
      details: 'ปลาไหลย่างชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 369,
      price: 29,
      nameEng: 'SHIRATAMA KUSHI',
      nameThai: 'ชิราทามะ คูชิ',
      details: 'ชิราทามะชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 368,
      price: 29,
      nameEng: 'BANANA KUSHI',
      nameThai: 'บานาน่า คูชิ',
      details: 'กล้วยชุบแป้งทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'KUSHIKATSU',
      id: 382,
      price: 25,
      nameEng: 'TARTAR SAUCE',
      nameThai: 'ทาร์ทาร์ซอส',
      details: 'ทาร์ทาร์ซอส',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'THAI Dish',
      id: 783,
      price: 99,
      nameEng: 'SPICY SALMON SALAD',
      nameThai: 'สไปร์ซี่ แซลมอล สลัด',
      details: 'ยำปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'THAI Dish',
      id: 784,
      price: 99,
      nameEng: 'SPICY FRIED SALMON SALAD',
      nameThai: 'สไปร์ซี่ ฟราย แซลมอล',
      details: 'ยำปลาแซลมอนทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'THAI Dish',
      id: 793,
      price: 99,
      nameEng: 'SPICY TUNA SALAD',
      nameThai: 'สไปร์ซี่ ทูน่า สลัด',
      details: 'ยำปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'THAI Dish',
      id: 789,
      price: 159,
      nameEng: 'SALMON OMLET WITH RICE',
      nameThai: 'แซลมอล ออมเร็ท วิช ไรซ์',
      details: 'ข้าวไข่เจียวปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'THAI Dish',
      id: 792,
      price: 119,
      nameEng: 'KID´ｓ　PLTATE',
      nameThai: 'คิดส์ เพลท',
      details: 'ชุดอาหารเด็ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 622,
      price: 179,
      nameEng: 'TORITERI CHEESE KAMAMESHI',
      nameThai: 'โทริเทริ ชีส คามามิชิ',
      details: 'ข้าวหน้าไก่เทริยากิใส่ชีสหม้อไฟ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 623,
      price: 179,
      nameEng: 'SALMON KINOKO KAMAMESHI',
      nameThai: 'แซลมอล กิโนะโกะ คามามิชิ',
      details: 'ข้าวหน้าปลาแซลมอนและเห็ดหม้อไฟ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 624,
      price: 179,
      nameEng: 'HOTATE KAMAMESHI',
      nameThai: 'โฮตาเตะ คามามิชิ',
      details: 'ข้าวหน้าหอยเชลล์หม้อไฟ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 601,
      price: 169,
      nameEng: 'TORI TERI DON',
      nameThai: 'โทริเทริ ดน',
      details: 'ข้าวหน้าไก่ย่างเทริยากิ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 612,
      price: 159,
      nameEng: 'OYAKO DON',
      nameThai: 'โอยาโกะ ดน',
      details: 'ข้าวหน้าไก่ราดไข่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 613,
      price: 189,
      nameEng: 'KATSU DON',
      nameThai: 'คัตสึ ดน',
      details: 'ข้าวหน้าหมูทอดราดไข่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 600,
      price: 249,
      nameEng: 'KAISEN DON',
      nameThai: 'ไคเซน ดน',
      details: 'ข้าวหน้าปลาดิบรวม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 610,
      price: 749,
      nameEng: 'UNAJYU',
      nameThai: 'อุนายุ',
      details: 'ข้าวหน้าปลาไหลย่าง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 602,
      price: 279,
      nameEng: 'SALMON DON',
      nameThai: 'แซลมอล ดน',
      details: 'ข้าวหน้าปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 603,
      price: 269,
      nameEng: 'SALMON EBIKO DON',
      nameThai: 'แซลมอล อิบิโกะ ดน',
      details: 'ข้าวหน้าปลาแซลมอนและไข่กุ้ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 606,
      price: 199,
      nameEng: 'MAGURO DON',
      nameThai: 'มากุโร่ ดน',
      details: 'ข้าวหน้าปลาทูน่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 604,
      price: 329,
      nameEng: 'SALMON IKURA DON',
      nameThai: 'แซลมอล อิกุระ ดน',
      details: 'ข้าวหน้าปลาแซลมอนและไข่ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 608,
      price: 679,
      nameEng: 'IKURA DON',
      nameThai: 'อิกุระ ดน',
      details: 'ข้าวหน้าไข่ปลาแซลมอน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 614,
      price: 249,
      nameEng: 'BEEF SUKIYAKI DON',
      nameThai: 'บีฟ สุกี้ยากี้ ดน',
      details: 'ข้าวหน้าเนื้อต้ม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 750,
      price: 39,
      nameEng: 'GOHAN',
      nameThai: 'โกฮัน',
      details: 'ข้าวสวย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 751,
      price: 29,
      nameEng: 'MISO SOUP',
      nameThai: 'มิโซะ',
      details: 'ซุปมิโซะ เต้าหู้กับสาหร่ายวากาเมะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 753,
      price: 99,
      nameEng: 'MENTAI GOHAN',
      nameThai: 'เมนไต โกฮัน',
      details: 'ข้าวหน้าไข่ปลาเมนไตโกะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SYOKUJI',
      id: 754,
      price: 89,
      nameEng: 'ONIGIRI SALMON',
      nameThai: 'โอนิกิริ แซลมอล',
      details: 'ข้าวปั้นโอนิกิริ ไส้ปลาแซลมอนรมควัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 704,
      price: 129,
      nameEng: 'ZARU SOBA',
      nameThai: 'ซารุ โซบะ',
      details: 'โซบะเย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 709,
      price: 129,
      nameEng: 'ZARU UDON',
      nameThai: 'ซารุุ ดน',
      details: 'อุด้งเย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 705,
      price: 249,
      nameEng: 'TENZARU',
      nameThai: 'เทนซารุ',
      details: 'โซบะเย็นพร้อมเทมปุระรวม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 700,
      price: 159,
      nameEng: 'KITSUNE UDON',
      nameThai: 'คิสึนึ อูดง',
      details: 'อุด้งร้อนเต้าหู้ทอด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 706,
      price: 199,
      nameEng: 'TEMPURA SOBA',
      nameThai: 'เทมปุระ โซบะ',
      details: 'โซบะร้อนเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 701,
      price: 199,
      nameEng: 'TEMPURA UDON',
      nameThai: 'เทมปุุระ อูดง',
      details: 'อุด้งร้อนเทมปุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 703,
      price: 189,
      nameEng: 'MENTAI UDON',
      nameThai: 'เมนไต อูดง',
      details: 'อุด้งคลุกไข่ปลาเมนไตโกะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'MENRUI',
      id: 707,
      price: 169,
      nameEng: 'SAUCE YAKI SOBA',
      nameThai: 'ซอส ยากิ โซบะ',
      details: 'ผัดยากิโซบะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 900,
      price: 49,
      nameEng: 'VANILLA ICECREAM',
      nameThai: 'วนิลา ไอศครีม',
      details: 'ไอศครีมวนิลา',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 901,
      price: 49,
      nameEng: 'LIME SHERBET',
      nameThai: 'เลม่อน เชอร์เบต',
      details: 'ไอศครีมมะนาว(เชอร์เบต)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 902,
      price: 59,
      nameEng: 'GREEN TEA ICECREAM',
      nameThai: 'กรีนที ไอศครีม',
      details: 'ไอศครีมชาเขียว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 903,
      price: 99,
      nameEng: 'SHIRATAMA ZENZAI',
      nameThai: 'ชิราทามะ เซนไซ',
      details: 'ถั่วแดงร้อนใส่โมจิชิราทามะ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 906,
      price: 119,
      nameEng: 'YUKIMI DAIFUKU CHOCOLATE',
      nameThai: 'ยุกิมิ ไดฟูกุ ช็อกโกแลต',
      details: 'ไอศครีมโมจิซอสครีมช็อคโกแลต',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 907,
      price: 119,
      nameEng: 'YUKIMI DAIFUKU AZUKI',
      nameThai: 'ยุุกิมิ ไดฟูกุ อะซูกิ',
      details: 'ไอศครีมโมจิถั่วแดง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'DESSERT',
      id: 910,
      price: 99,
      nameEng: 'FRUIT CALPIS SHERBET',
      nameThai: 'ฟรุต คาลพิส เชอร์เบต',
      details: 'เชอร์เบตคาลพิสมิกซ์เบอร์รี่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'BEER',
      id: 50,
      price: 85,
      nameEng: 'LEO BEER SMALL',
      nameThai: 'ลีโอเบียร์ สมอล',
      details: 'เบียร์ลีโอขวดเล็ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'BEER',
      id: 51,
      price: 135,
      nameEng: 'LEO BEER LARGE',
      nameThai: 'ลีโอเบียร์ ลาร์จ',
      details: 'เบียร์ลีโอขวดใหญ่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'BEER',
      id: 52,
      price: 95,
      nameEng: 'SHINGHA BEER SMALL',
      nameThai: 'สิงห์เบียร์ สมอล',
      details: 'เบียร์สิงห์ขวดเล็ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'BEER',
      id: 53,
      price: 145,
      nameEng: 'SHINGHA BEER LARGE',
      nameThai: 'สิงห์เบียร์ ลาร์จ',
      details: 'เบียร์สิงห์ขวดใหญ่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPAN BRAND BEER',
      id: 57,
      price: 120,
      nameEng: 'KIRIN ICHIBANSHIBORI SMALL',
      nameThai: 'คิริน อิชิบินชิโบริ สมอล',
      details: 'เบียร์คิรินอิจิบันชิโบริขวดเล็ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPAN BRAND BEER',
      id: 58,
      price: 210,
      nameEng: 'KIRIN ICHIBANSHIMORI LARGE',
      nameThai: 'คิรินอิชิบินชิโบริ ลาร์จ',
      details: 'เบียร์คิรินอิจิบันชิโบริขวดใหญ่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPAN BRAND BEER',
      id: 56,
      price: 175,
      nameEng: 'SAPPORO BEER SMALL',
      nameThai: 'ซัปโปโรเบียร์ สมอล',
      details: 'เบียร์ซัปโปโรขวดเล็ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 60,
      price: 139,
      nameEng: 'SAKE HOT',
      nameThai: 'สาเก ฮอต',
      details: 'สาเกร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 61,
      price: 299,
      nameEng: 'SAKE COLD KIZAKURA',
      nameThai: 'สาเก คิซากุระ',
      details: 'สาเกคิซากุระ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 63,
      price: 499,
      nameEng: 'SAKE COLD KIKUSUI JUNMAIGINJO',
      nameThai: 'สาเก คิคูซุยจุนไมจินโจ',
      details: 'สาเก คิคูซุยจุนไมจินโจ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 66,
      price: 399,
      nameEng: 'SAKE COLD HOURAISEN',
      nameThai: 'สาเกโคลโฮราเซ็น',
      details: 'สาเกโฮราอิเซน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 67,
      price: 499,
      nameEng: 'SPARKLING SAKE MIO',
      nameThai: 'สปาร์คกลิ้งสาเก มิโอ',
      details: 'สปาร์คกลิ้งสาเก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE SAKE',
      id: 65,
      price: 159,
      nameEng: 'MASU SAKE',
      nameThai: 'มัสสึ สาเก',
      details: 'มัตสึสาเก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 71,
      price: 159,
      nameEng: 'MAI TAI',
      nameThai: 'ไหมไทย',
      details: 'ไหมไทย',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 72,
      price: 139,
      nameEng: 'PINA COLADA',
      nameThai: 'พีน่า โคลาด้า',
      details: 'พีน่าโคลาด้า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 73,
      price: 169,
      nameEng: 'MOJITO',
      nameThai: 'โมจิโต้',
      details: 'โมจิโต้',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 74,
      price: 229,
      nameEng: 'CAMPARI MOJITO',
      nameThai: 'คัมพะรี โมจิโต้',
      details: 'คัมพารี่　โมจิโต้',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 75,
      price: 229,
      nameEng: 'UMESYU MOJITO',
      nameThai: 'อุเมะชุโมจิโต้',
      details: 'อุเมะชุ โมจิโต้',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 79,
      price: 139,
      nameEng: 'PEACH SANGRIA BLANCA',
      nameThai: 'พีช ซังเกียร์ บลานคา',
      details: 'พีชซังเกียร์บลานคา',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COCKTAILS',
      id: 80,
      price: 169,
      nameEng: 'UOTERU ORIGINAL ',
      nameThai: 'อุุโอเทรุออริจิเนลแอนด์ยูซุ',
      details: 'อุโอเทรุ ออริจิเนล รัมแอนด์ยูซุ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: '(Shochu&Soda)',
      id: 81,
      price: 109,
      nameEng: 'FRESH LEMON SOUR',
      nameThai: 'เฟรชเลม่อนซาวร์',
      details: 'เฟรชเลมอนซาวร์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: '(Shochu&Soda)',
      id: 82,
      price: 109,
      nameEng: 'YUZU SOUR',
      nameThai: 'ยูซุซาวร์',
      details: 'ยูซุซาวร์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: '(Shochu&Soda)',
      id: 84,
      price: 109,
      nameEng: 'PEACH SOUR',
      nameThai: 'พีชซาวร์',
      details: 'พีชซาวร์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'JAPANESE WHISKEY',
      id: 88,
      price: 139,
      nameEng: 'WHISKEY HIGHBALL',
      nameThai: 'วิสกี้ไฮบอล',
      details: 'วิสกี้ไฮบอล',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'UMESHU WINE',
      id: 85,
      price: 119,
      nameEng: 'UMESHU (PLUM LIQUEUR)',
      nameThai: 'อุเมะชุุ',
      details: 'อุเมะชุ (เหล้าบ๊วย)',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'UMESHU WINE',
      id: 89,
      price: 119,
      nameEng: 'UMESHU with SODA',
      nameThai: 'อุเมะชุ วิช โซดา',
      details: 'เหล้าบ๊วยผสมโซดา',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'UMESHU WINE',
      id: 86,
      price: 139,
      nameEng: 'RED WINE GLASS',
      nameThai: 'เรด ไวน์ กลาส',
      details: 'ไวน์แดง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'UMESHU WINE',
      id: 87,
      price: 139,
      nameEng: 'WHITE WINE GLASS',
      nameThai: 'ไวท์ไวน์กลาส',
      details: 'ไวน์ขาว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 31,
      price: 99,
      nameEng: 'MANGO SMOOTHIE',
      nameThai: 'แมงโก้ สมูทตี้',
      details: 'มะม่วงปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 32,
      price: 99,
      nameEng: 'WATER MELON SMOOTHIE',
      nameThai: 'วอเตอร์เมล่อน สมูทตี้',
      details: 'แตงโมปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 33,
      price: 99,
      nameEng: 'BANANA SMOOTHIE',
      nameThai: 'บานาน่า สมูทตี้',
      details: 'กล้วยปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 35,
      price: 99,
      nameEng: 'PINEAPPLE SMOOTHIE',
      nameThai: 'พายแนปเปิ้ล สมูทตี้',
      details: 'สับปะรดปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 38,
      price: 99,
      nameEng: 'CALPIS SMOOTHIE',
      nameThai: 'คาลพีช สมูทตี้',
      details: 'คาลพีสแลคโตะปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 39,
      price: 99,
      nameEng: 'MIXED BERRY SMOOTHIE',
      nameThai: 'มิกซ์ เบอรี่ สมูทตี้',
      details: 'มิกส์เบอร์รี่ปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SMOOTHIE',
      id: 37,
      price: 169,
      nameEng: 'AVOCADO SMOOTHIE',
      nameThai: 'อะโวคาโด สมูทตี้',
      details: 'อะโวคาโดปั่น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 10,
      price: 29,
      nameEng: 'HOT GREEN TEA',
      nameThai: 'ฮอต กรีน ที',
      details: 'ชาเขียวร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 11,
      price: 29,
      nameEng: 'ICE GREEN TEA',
      nameThai: 'ไอซ์ กรีน ที',
      details: 'ชาเขียวเย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 23,
      price: 29,
      nameEng: 'BARLEY TEA COLD',
      nameThai: 'บาร์เล่ย์ ที โคล',
      details: 'ชาข้าวสาลี',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 12,
      price: 50,
      nameEng: 'COLA',
      nameThai: 'โคล่า',
      details: 'โค้ก',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 13,
      price: 60,
      nameEng: 'COLA ZERO',
      nameThai: 'โคล่า ซีโร่',
      details: 'โค้กซีโร่',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 19,
      price: 50,
      nameEng: 'SPRITE',
      nameThai: 'สไปรท์',
      details: 'สไปรท์',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 14,
      price: 50,
      nameEng: 'ORANGE JUICE',
      nameThai: 'ออเร้นท์ จูส',
      details: 'น้ำส้ม',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 15,
      price: 50,
      nameEng: 'PINEAPPLE JUICE',
      nameThai: 'พายแอปเปิ้ิ้ล จูส',
      details: 'น้ำสับปะรด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 16,
      price: 50,
      nameEng: 'SODA+MANAO',
      nameThai: 'โซดามะนาว',
      details: 'โซดามะนาว',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 17,
      price: 50,
      nameEng: 'SODA BOTTLE',
      nameThai: 'โซดาขวด',
      details: 'โซดาขวด',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 18,
      price: 50,
      nameEng: 'LIPTON',
      nameThai: 'ลิปตัน',
      details: 'ลิปตัน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 21,
      price: 35,
      nameEng: 'MINERAL WATER',
      nameThai: 'มิเนเร่ วอเตอร์',
      details: 'น้ำเปล่า',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 22,
      price: 40,
      nameEng: 'ICE',
      nameThai: 'ไอซ์',
      details: 'น้ำแข็ง',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'SOFT DRINK',
      id: 26,
      price: 70,
      nameEng: 'COFFEE FLOAT',
      nameThai: 'คอฟฟี่ โฟส',
      details: 'ไอศครีมกาแฟ',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOT',
      id: 90,
      price: 69,
      nameEng: '',
      nameThai: 'คาเฟ่ อเมริกาโน่ ฮอต',
      details: 'กาแฟอเมริกาโน่ร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    
    {
      type: 'HOT',
      id: 93,
      price: 79,
      nameEng: '',
      nameThai: 'คาเฟ่ ลาเต้ ฮอต',
      details: 'กาแฟลาเต้ร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOT',
      id: 98,
      price: 79,
      nameEng: '',
      nameThai: 'กรีนที ลาเต้ ฮอต',
      details: 'กรีนทีลาเต้ร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'HOT',
      id: 94,
      price: 79,
      nameEng: '',
      nameThai: 'คาปูชิโน่ ฮอต',
      details: 'คาปูชิโน่ร้อน',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COL DOUBLE SHOT',
      id: 95,
      price: 109,
      nameEng: '',
      nameThai: 'คาเฟ่อเมริกาโน่ โคล',
      details: 'กาแฟอเมริกาโน่เย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COL DOUBLE SHOT',
      id: 96,
      price: 119,
      nameEng: '',
      nameThai: 'คาเฟ่ ลาเต้ โคล',
      details: 'กาแฟลาเต้เย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COL DOUBLE SHOT',
      id: 99,
      price: 119,
      nameEng: '',
      nameThai: 'กรีน ที ลาเต้ โคล',
      details: 'กรีนทีลาเต้เย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    },
    {
      type: 'COL DOUBLE SHOT',
      id: 97,
      price: 119,
      nameEng: '',
      nameThai: 'คาปููชิโน่ โคล',
      details: 'คาปูชิโน่เย็น',
      img: 'https://www.shichi.co.th/wp-content/uploads/2021/06/Matsu-Sashimi.jpg'
    }
  ])
}

export default data
