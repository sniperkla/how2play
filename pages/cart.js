import React, { useState } from 'react'
import Link from 'next/link'
import { useSelector, useDispatch } from 'react-redux'

const cart = () => {
  const dispatch = useDispatch()
  const { data } = useSelector((state) => ({ ...state }))
  const { price } = useSelector((state) => ({ ...state }))

  console.log(data)
  console.log(price)

  const result = Object.assign(data, price)
  console.log(result)

  dispatch({
    type: 'RESULT',
    payload: result
  })

  var i = 0
  var k = 0
  // storage previous data

  if (typeof window !== 'undefined') {
    localStorage.setItem('item' + localStorage.getItem('key'), result.name)

    console.log(localStorage.getItem('item5'))
    //  (localStorage.setItem('item'+'1')) // back
  }
  return (
    <div>
      <Link href="/">
        <a className="notification">
          <span style={{ float: 'right' }}>Home</span>
          <span className="badge">{1}</span>
        </a>
      </Link>
      <h1>
        ราคา : {result.price}
        ชื่อสินค้า : {result.name}
        ราคารวม : {result.all}
      </h1>
    </div>
  )
}
export default cart
