import React, { useState } from 'react'
import data from './data'
import Autocomplete from '@mui/material/Autocomplete'
import TextField from '@mui/material/TextField'
import { green } from '@mui/material/colors'
import { display } from '@mui/system'
import Link from 'next/link'
import { useSelector, useDispatch } from 'react-redux'

export default function Home() {
  var nameth = data().map((data1) => data1.nameThai)
  var nameeng = data().map((data4) => data4.nameEng)
  var price = data().map((data2) => data2.price)
  var img = data().map((data3) => data3.img)
  var names = nameth
  var options = names
  var [count, setCount] = useState(1)
  // var [name, setName] = useState('') // vale
  var choose = 0
  var image = ''
  var [value, setValue] = useState('')
  var [inputValue, setInputValue] = useState('')

  // const Namechange = (event) => {
  //   setName(event.target.value)
  // }

  const dispatch = useDispatch()
  const inc = () => {
    setCount(count + 1)
  }
  if (count < 1) {
    count = 1
  }
  const dec = () => {
    setCount(count - 1)
  }

  var i = 0
  for (var x = 0; x < nameth.length; x++) {
    if (value === nameth[i] || value === nameeng[i]) {
      if (img[i] === '') {
        img[i] =
          'https://bk.asia-city.com/sites/default/files/styles/og_fb/public/25_baht_sushi-1.jpg?itok=S6IPg0D_'
      } else {
        image = img[i]
      }
      choose = price[i]
    } else i++
  }
  var all = choose * count
  const full = { price: price[i], name: nameth[i] }
   if (typeof window !== 'undefined') {
      if(localStorage.getItem('key')===null||localStorage.getItem('key')=== NaN||localStorage.getItem('itemNaN')||localStorage.getItem('itemnull')){
       localStorage.clear()
       localStorage.setItem('key',0)
      }
      var counter = localStorage.getItem('key')
    }


  
  const handleSubmit = (event) => {

    if (typeof window !== 'undefined') {
      
      counter = 1 + parseInt(localStorage.getItem('key'))
      localStorage.setItem('item'+counter, nameth[i])
      localStorage.setItem('key', counter)
    }
    event.preventDefault()



    dispatch({
      type: 'DETAILFOOD',
      payload: full
    })
    const funcount = { count: count, all: all }
    dispatch({
      type: 'DETAILPRICE',
      payload: funcount
    })
  }

  return (
    <div>
      <Link href="/cart">
        <a className="notification">
          <span style={{ float: 'right' }}>รายการ</span>
          <span className="badge">{count}</span>
        </a>
      </Link>
      <h1 style={{ color: 'bule', textAlign: 'center' }}>ร้านอาหารแห่งหนึ่ง</h1>
      {/* <input type="text" name="quility" onChange={Namechange} value={name} /> */}
      <center>
        {' '}
        <Autocomplete
          value={value}
          onChange={(event, newValue) => {
            
            setValue(newValue)
          }}
          inputValue={inputValue}
          onInputChange={(event, newInputValue) => {
            setInputValue(newInputValue)
          }}
          id="controllable-states-demo"
          options={options}
          sx={{ width: 300 }}
          renderInput={(params) => <TextField {...params} label="เลือกเมนู" />}
        />{' '}
      </center>
      <h3> จำนวน : x{count} </h3> <center> ราคารวม : {all} บาท </center>
      <td>
        <button onClick={inc} className="btn btn-2 btn-sep icon-info">
          เพิ่ม
        </button>
      </td>
      <td>
        <button onClick={dec} className="btn btn-3 btn-sep icon-cart">
          ลด
        </button>
      </td>
      <td>
        <button onClick={dec} className="btn btn-1 btn-sep icon-send">
          สั่ง
        </button>
      </td>
      <center>
        <img src={image} width="300" height="300" alt="Italian Trulli" />
      </center>
      <form onSubmit={handleSubmit}>
        <center>
          {' '}
          <input className="btn btn-1 btn-sep icon-send" type="submit" />
        </center>
      </form>
    </div>
  )
}
