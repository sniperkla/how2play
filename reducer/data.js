export const userData = (state = {}, action) => {
  switch (action.type) {
    case 'DETAILFOOD':
      return action.payload

    default:
      return state
  }
}

export const userPrice = (state = {}, action) => {
  switch (action.type) {
    case 'DETAILPRICE':
      return action.payload

    default:
      return state
  }
}
export const userResult = (state = {}, action) => {
  switch (action.type) {
    case 'DETAILRESULT':
      return action.payload

    default:
      return state
  }
}
