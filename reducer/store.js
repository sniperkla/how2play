import { combineReducers } from "redux";
import { userData } from "./data"; // store1
import { userPrice } from "./data";
import { userResult } from "./data";
//combine store

const rootReducer = combineReducers({
    data : userData, // store1
    price : userPrice,
    result : userResult
})

export default rootReducer